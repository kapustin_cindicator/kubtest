# Эксперименты с запуском kubernetes jobs из пода

## TODO:
* Добавить общий volume DONE
* Поменять job - вместо echo пусть создает файл DONE
* Прикрутить лужди
* разделить неймспейсы. Или нет?.. DONE
* использовать service account (не default) DONE


## Подготовка окружения

```
minikube start
```
```
eval $(minikube docker-env)
```

## Настройка job

```
cd job
```
```
docker build . -t job:1.0
```

## Настройка сервера
```
cd server
```
```
docker build . -t simpleserver:1.0
```

## Деплой в кластер
```
cd kubernetes
```
```
# создает namespace = server-namespace
kubectl apply -f server-namespace.yaml
```
```
# создает аккаунт = job-manager
kubectl apply -f server-service-account.yaml
```
```
# Добавляет роль job-manager для неймспейса server-namespace (имеет доступ к подам и задачам)
kubectl apply -f server-role.yaml
```
```
# Создает binding job-manager (job-manager - job-manager)
kubectl apply -f server-role-binding.yaml
```

```
kubectl apply -f server-deployment.yaml
```
```
kubectl apply -f server-service.yaml
```

## Пробуем запускать job
```
# Получим адрес:
minikube service list | grep server-service
```
```
curl http://addr:port/my-test-job-uid - должен вернуть:

Trying create job with uid: my-test-job-uid
Pods in default
simple-server-6575d7cdf5-l5g4c
Jobs not found
Jobs created%
```
```
# При повторном обращении должен отдавать:
Trying create job with uid: my-test-job-uid
Pods in default
simple-server-6575d7cdf5-l5g4c
test-job-my-test-job-uid-sftmf
job:
{'metadata': {'name': 'test-job-my-test-job-uid', 'namespace': 'default', 'selfLink': '/apis/batch/v1/namespaces/default/jobs/test-job-my-test-job-uid', 'uid': 'c73175b8-fc7f-11e8-bafa-080027beaf99', 'resourceVersion': '578288', 'creationTimestamp': '2018-12-10T13:30:36Z', 'labels': {'spawned_by': 'server', 'test_task_id': 'my-test-job-uid'}}, 'spec': {'parallelism': 1, 'completions': 1, 'backoffLimit': 1, 'selector': {'matchLabels': {'controller-uid': 'c73175b8-fc7f-11e8-bafa-080027beaf99'}}, 'template': {'metadata': {'name': 'test-job', 'creationTimestamp': None, 'labels': {'controller-uid': 'c73175b8-fc7f-11e8-bafa-080027beaf99', 'job-name': 'test-job-my-test-job-uid'}}, 'spec': {'containers': [{'name': 'test-job', 'image': 'job:1.0', 'command': ['echo', 'hello', 'world!'], 'resources': {}, 'terminationMessagePath': '/dev/termination-log', 'terminationMessagePolicy': 'File', 'imagePullPolicy': 'IfNotPresent'}], 'restartPolicy': 'Never', 'terminationGracePeriodSeconds': 30, 'dnsPolicy': 'ClusterFirst', 'securityContext': {}, 'schedulerName': 'default-scheduler'}}}, 'status': {'conditions': [{'type': 'Complete', 'status': 'True', 'lastProbeTime': '2018-12-10T13:30:39Z', 'lastTransitionTime': '2018-12-10T13:30:39Z'}], 'startTime': '2018-12-10T13:30:36Z', 'completionTime': '2018-12-10T13:30:39Z', 'succeeded': 1}}% 
```


```
# Убедимся что джоба запустилась
kubectl get pods | grep test-job
▶ kubectl logs test-job-asdf-qzz57 
hello world!
```
```
# Удаляем все jobs
kubectl delete jobs.batch --all
```
```
# переключаемя на работу в default namespace
kubectl config set-context $(kubectl config current-context) --namespace=default
# Проверим
kubectl config view | grep namespace
```
```
# Удаляем пользователей
kubectl delete serviceaccounts --all
```
```
# Удаляем роли и биндинги
kubectl delete rolebindigs --all
```
```
kubectl delete role --all
```

# Alembic
```
Initializtion - dont do this
alembic init alembic
```
```
Initital commit
alembic revision --autogenerate -m 'initial'
```
```
show migrations
alembic history --verbose
```
```
upgrade
alembic upgrade head
```
