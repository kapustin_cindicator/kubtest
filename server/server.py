from aiohttp import web
import pykube
import operator

server_namespace = 'server-namespace'


def create_spec(job_uid):
    spec = {
        "volumes": [{
            "name": "pv1",
            "hostPath": {
                "path": "/pv1"
            },
        }],
        "containers": [{
            "name": "test-job",
            "image": "job:1.0",
            "volumeMounts": [{
                "name": "pv1",
                "mountPath": '/data'
            }],
            "command": ["touch",  "/data/file.txt"],
        }],
        "restartPolicy": "Never"
    }

    # Render job
    job_json = {
        "apiVersion": "batch/v1",
        "kind": "Job",
        "metadata": {
            "name": 'test-job-%s' % job_uid,
            "namespace": server_namespace,
            "labels": {
                "spawned_by": "server",
                "test_task_id": job_uid
            }
        },
        "spec": {
            "backoffLimit": 1,
            "template": {
                "metadata": {
                    "name": 'test-job'
                },
                "spec": spec
            }
        }
    }
    return job_json

async def handle(request):
    # get requets
    job_uid = request.match_info.get('text', "123321")
    output = ['Trying create job with uid: %s' % job_uid]

    # get pods info
    config = pykube.KubeConfig.from_service_account()
    print('config:', config)
    api = pykube.HTTPClient(config)
    pods = pykube.Pod.objects(api).filter(namespace=server_namespace)
    ready_pods = filter(operator.attrgetter("ready"), pods)
    output.append('Pods in %s' % server_namespace)
    for pod in pods:
        output.append(str(pod))

    # state of jobs
    jobs = pykube.Job.objects(api) \
                     .filter(namespace=server_namespace) \
                     .filter(selector="test_task_id=" + job_uid) \
                     .response['items']
    if len(jobs) != 1:
        output.append('Jobs not found')
    else:
        output.append('job:')
        output.append(str(jobs[0]))

    # start new job
    if len(jobs) == 0:
        job_json = create_spec(job_uid)
        job = pykube.Job(api, job_json)
        job.create()
        print('job staeted')
        output.append('Jobs created')

    text = '\n'.join(output)
    return web.Response(text=text)

app = web.Application()
app.add_routes([web.get('/', handle),
                web.get('/{text}', handle)])

web.run_app(app, port=8081)
