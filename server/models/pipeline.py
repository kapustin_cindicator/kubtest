from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime, Float, Numeric, JSON, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime

Base = declarative_base()

#### IN DATA

class Event(Base):
    __tablename__ = 'event'

    id = Column(Integer, primary_key=True)
    end_time = Column(DateTime, index=True, nullable=False) # set index
    real_value = Column(Numeric(11, 5))


class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String)
    first_name = Column(String)
    last_name = Column(String)


class UserAnswer(Base):
    __tablename__ = 'user_answer'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    event_id = Column(Integer, ForeignKey('event.id'), nullable=False)
    created_at = Column(DateTime, nullable=False)
    price_value = Column(Numeric(11, 5))

    event = relationship("Event", lazy='joined', backref="user_answers")
    user = relationship("User", lazy='joined', backref="user_answers")


###

class MLModel(Base):
    __tablename__ = 'ml_model'

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    params = Column(JSON, nullable=False)


class ForwardTest(Base):
    __tablename__ = 'forward_test'

    id = Column(Integer, primary_key=True)
    event_id = Column(Integer, ForeignKey('event.id'), nullable=False)
    event = relationship("Event", lazy='joined', backref="forward_tests")

    ml_model_id = Column(Integer, ForeignKey('ml_model.id'), nullable=False)
    ml_model = relationship("MLModel", backref="forward_test")

    predict_value = Column(Numeric(11, 5), nullable=False)


class BackTest(Base):
    __tablename__ = 'back_test'

    id = Column(Integer, primary_key=True)
    event_id = Column(Integer, ForeignKey('event.id'), nullable=False)
    event = relationship("Event", backref="back_tests")

    ml_model_id = Column(Integer, ForeignKey('ml_model.id'), nullable=False)
    ml_model = relationship("MLModel", backref="back_test")


    created_at = Column(DateTime, nullable=False, default=datetime.utcnow)
    predict_value = Column(Numeric(11, 5), nullable=False)


#### Files

class File(Base):
    __tablename__ = 'file'

    id = Column(Integer, primary_key=True)
    path = Column(String, nullable=False)
    meta = Column(JSON)
