import luigi
from models.pipeline import Event, User, UserAnswer
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import os
from datetime import timedelta
from datetime import datetime

engine = create_engine(os.environ.get('PIPELINE_DB_URI'))
Session = sessionmaker()
Session.configure(bind=engine)
session = Session()


class UserAnswerTarget(luigi.Target):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def exists(self):
        events_count = session.query(Event).filter(
            Event.end_time>=str(self.start),
            Event.end_time<=str(self.end)).count()

        return bool(events_count)



class PrepareData(luigi.Task):
    start = luigi.DateParameter()
    end = luigi.DateParameter()

    def output(self):
        return UserAnswerTarget(self.start, self.end)

    def run(self):
        users = []
        user1 = User()
        user1.email = 'a@a.a'
        user1.first_name = 'a'
        user1.last_name = 'A'
        users.append(user1)
        session.add(user1)

        user2 = User()
        user2.email = 'b@b.b'
        user2.first_name = 'b'
        user2.last_name = 'B'
        users.append(user2)
        session.add(user2)

        events = []
        event1 = Event()
        event1.end_time = self.start + timedelta(days=1)
        event1.real_value = 4.55
        events.append(event1)
        session.add(event1)

        event2 = Event()
        event2.end_time = self.start + timedelta(days=2)
        event2.real_value = 5.55
        events.append(event2)
        session.add(event2)

        for event in events:
            for user in users:
                ua = UserAnswer()
                ua.event = event
                ua.user = user
                ua.created_at = datetime(2018, 3, 3)
                ua.price_value = 3.14
                session.add(ua)
        session.commit()



class BinaryDataTarget(luigi.Target):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def exists(self):
        meta = {
            'file_type': 'BinaryDF',
            'start': self.start,
            'end': self.end,
        }
        file_exists = session.query(File).filter(
            File.meta=meta).first()

        return bool(file_exists)



class PrepareData(luigi.Task):
    start = luigi.DateParameter()
    end = luigi.DateParameter()

    def output(self):
        return UserAnswerTarget(self.start, self.end)

    def run(self):
 










































#### kubernetes
import kubernetes
import os


#kubernetes_config = kubernetes.kubernetes(auth_method='service-account',
#                               max_retrials=0)

class CreateFile(kubernetes.KubernetesJobTask):
    filename = luigi.Parameter(
        default="filename.txt",
        description="filename")
    __POLL_TIME = 5 # seconds for polling job

    @property
    def labels(self):
        labels = {
            'job_type': 'create-file'
        }
        return labels

    @property
    def spec_schema(self):
        spec_schema = {
            "volumes": [{
                "name": "pv1",
                "hostPath": {
                    "path": "/pv1"
                },
            }],
            "containers": [{
                "name": "test-job",
                "image": "job:1.0",
                "volumeMounts": [{
                    "name": "pv1",
                    "mountPath": '/data'
                }],
                "command": ["touch",  "/data/%s" % self.filename],
            }],
            "restartPolicy": "Never"
        }
        return spec_schema


    def output(self):
        return luigi.LocalTarget(os.path.join('/data', self.filename))


class CountFiles(kubernetes.KubernetesJobTask):
    files_number = luigi.IntParameter('default=10', description='number of files')

    def requires(self):
        return [CreateFile(filename='filename_%d.txt' % i) for i in range(self.files_number)]

    @property
    def labels(self):
        labels = {
            'job_type': 'count-files'
        }
        return labels

    @property
    def spec_schema(self):
        spec_schema = {
            "volumes": [{
                "name": "pv1",
                "hostPath": {
                    "path": "/pv1"
                },
            }],
            "containers": [{
                "name": "test-job",
                "image": "job:1.0",
                "volumeMounts": [{
                    "name": "pv1",
                    "mountPath": '/data'
                }],
                "command": ["ls",  "/data" , "|", "wc", "-l"],
            }],
            "restartPolicy": "Never"
        }
        return spec_schema


    def output(self):
        pass
