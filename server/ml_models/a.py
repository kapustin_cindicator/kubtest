from datetime import datetime

class MLModel:
    def __init__(self, params, model, source):
        v

class MLLearner:
    pass

class MLPredictor:
    pass

class DfSchema(DFSchema):
    pass

class ParamsSchema(Schema):
    pass

class MyModel(MLModel):

    fit_params_schema = ParamsSchema()
    fit_df_schema = DFSchema()
    def fit(self, params, df):
        return self

    def serialize(self):
        return BufferIO

    def predict(self, params, df):
        return df


class MyModelPredictor(MLPredictor):

    params_schema = ParamsSchema()
    in_schema = DFSchema()
    out_schema = DFSchema()

    def __init__(self, params, model, source):
        self.params = params
        self.source = source

    def predict(self, df):
        return df2

class MyModelLearner(MLLearner):

    params_schema = ParamsSchema()
    in_schema = DFSchema()
    ml_model = MyModel

    def __init__(self, params, source):
        self.params = params
        self.source = source

    def learn(self, df):
        return MyModel()



period_uas = PeriodUserAnswers(start=datetime(2017, 1, 1), end=datetime(2018, 1, 1))
uas = UserAnswers(event_id=123)
my_model = MyModelLearner(params={'a': 1}, source=period_uas)
predict = MyModel(params={'a': 1}, model=my_model, source=uas)
