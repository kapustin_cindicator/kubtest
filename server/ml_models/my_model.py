from abc import ABC, abstractmethod
from sources import PeriodUserAnswers, UserAnswers
from ds import DSchema, DSColumn, DSIndex

class MLModel(ABC):

    @abstractmethod
    def fit(self):
        pass

    @abstractmethod
    def load(self):
        pass

    @abstractmethod
    def dump(self, buff):
        pass


class MLLearner(ABC):

    @staticmethod
    @abstractmethod
    def params_cls(self):
        pass


    @staticmethod
    @abstractmethod
    def ml_model(self):
        pass

    @staticmethod
    @abstractmethod
    def sources_spec(self):
        pass

    def __init__(self, params, sources):
        self._PARAMS = params
        if not isinstance(sources, dict):
            raise ValueError('Sources must be dict')
        if set(sources.keys()) != set(self.sources_spec.keys()):
            raise ValueError('Source and sources_spec mismatch. Source spec = %s. Sources = %s' % (list(self.sources_spec.keys()), list(sources.keys())))
        errors = []
        for source_name, source in sources.items():
            if source_name not in self.sources_spec:
                errors.append('%s source not in sorces_spec' % source_name)
            print(self.sources_spec[source_name]())
            print(source.out_schema())
            if not self.sources_spec[source_name]().issuperset(source.out_schema()):
                errors.append('Schema mismatch for %s %s %s' % (source_name, self.sources_spec[source_name](), source.out_schema()))
        if errors:
            raise ValueError(errors)
        self._SOURCES = sources

    def __repr__(self):
        return '%s(%s,%s)' % (self.__class__.__name__,
                              self._PARAMS.__repr__(),
                              self._SOURCES)
    def requirements(self):
        return list(self._SOURCES.values())



class MLPredictor(ABC):

    @staticmethod
    @abstractmethod
    def params_cls(self):
        pass

    @staticmethod
    @abstractmethod
    def learner(self):
        pass

    @staticmethod
    @abstractmethod
    def out_schema(self):
        pass

    @staticmethod
    @abstractmethod
    def sources_spec(self):
        pass

    def __init__(self, params, learner, sources):
        self._PARAMS = params
        if not isinstance(learner, self.learner):
            raise ValueError('Unknown learner')
        self._LEARNER = learner
        if set(sources.keys()) != set(self.sources_spec.kes()):
            raise ValueError('Source and sources_spec mismatch')
        errors = []
        for source, df in sources:
            if source not in self.sources_spec:
                raise ValueError('%s source not in sorces_spec' % source)
            error = self.sources_spec[source].validate(df)
            if error:
                errors.append(error)
        if errors:
            raise ValueError(errors)
        self._SOURCES = sources

    def __repr__(self):
        return '%s(%s,%s,%s)' % (self.__class__.__name__,
                                 self._PARAMS.__repr__(),
                                 self._LEARNER.__repr__(),
                                 self._SOURCES)
    def requirements(self):
        reqs = [self._LEARNER] + list(self._SOURCES.values())
        return reqs


class MyModel:
    def fit(self, params, data):
        pass

    def predict(self, params, data):
        pass

    def load(self):
        pass

    @abstractmethod
    def dump(self, buff):
        pass


@attr.s(kw_only=True)
class MyModelLearnerParams:
    param1 = attr.ib()


class PeriodUserAnswersSourceSchema(DSchema):

    class Meta:
        all_required = True

    index = DSIndex(data_type='int64')
    event_id = DSColumn(data_type='int64')
    user_id = DSColumn(data_type='int64')
    answer = DSColumn(data_type='float64')
    real = DSColumn(data_type='float64')



class MyModelLearner(MLLearner):
    params_cls = MyModelLearnerParams
    sources_spec = {
        'period_user_answers': PeriodUserAnswersSourceSchema
    }
    ml_model = MyModel



@attr.s(kw_only=True)
class MyModelPredicterParams:
    pass


class UserAnswersSourceSchema(DSchema):

    class Meta:
        all_required = True
        exclude_columns = ['real']

    index = DSIndex(data_type='int64')
    event_id = DSColumn(data_type='int64')
    user_id = DSColumn(data_type='int64')
    answer = DSColumn(data_type='float64')


class MyModelPredictorOutSchema(DSchema):

    class Meta:
        all_required = True

    index = DSIndex(data_type='int64')
    predict = DSColumn(data_type='float64')


class MyModelPredictor(MLPredictor):
    params_cls = MyModelPredicterParams
    sources_spec = {
        'period_user_answers': UserAnswersSourceSchema
    }
    learner = MyModelLearner
    out_schema = MyModelPredictorOutSchema



# test

puas = PeriodUserAnswers(PeriodUserAnswers.params_cls(end=datetime(2019, 3, 3),
                                                      start=datetime(2011, 3, 3)))
uas = UserAnswers(UserAnswers.params_cls(event_id='123124'))

import pandas as pd
period_user_answers = pd.DataFrame([
    {
        'event_id': 1,
        'user_id': 1,
        'answer': 1.0,
        'real': 1.0,
    }
])

learner = MyModelLearner(
    MyModelLearner.params_cls(param1=10), sources={'period_user_answers': puas})


