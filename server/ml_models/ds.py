from numpy import dtype
from abc import ABC, abstractmethod
import pandas as pd

class DSField:

    @property
    def dtype(self):
        return self._dtype

    @property
    def required(self):
        return self._required

    def __eq__(self, field):
        if not isinstance(field, self.__class__):
            return False # DSColumn != DSIndex
        if self.dtype != field.dtype:
            return False
        if self.required != field.required:
            return False
        return True


class DSIndex(DSField):
    def __init__(self, data_type):
        self._dtype = dtype(data_type)
        self._required = True


class DSColumn(DSField):

    def __init__(self, data_type, required=False):
        self._dtype = dtype(data_type)
        self._required = False


class DSMeta(type):

    def __new__(cls, name, bases, attrs):
        # update Meta
        cls_meta = attrs.pop('Meta', None)
        if cls_meta:
            attrs['_all_required'] = getattr(cls_meta, 'all_required', False)
            attrs['_exclude_columns'] = getattr(cls_meta, 'exclude_columns', [])

        columns = {}
        index = None

        # check exclude columns
        exclude_columns = attrs.get('_exclude_columns', None)
        if exclude_columns and 'index' in exclude_columns:
            raise ValueError('"index" name in exclude fields')
        for attr_name, attr in attrs.items():
            if not isinstance(attr, DSField):
                continue
            if attr_name == 'index':
                if not isinstance(attr, DSIndex):
                    raise ValueError('Column with "index" name')

            if isinstance(attr, DSIndex):
                if index is not None:
                    raise ValueError('Index %s is repeated')
                index = attr

            if isinstance(attr, DSColumn):
                if attr_name in columns:
                    raise ValueError('Column %s is repeated' % attr_name)
                columns[attr_name] = attr

        attrs['_df_columns'] = columns
        attrs['_df_index'] = index

        return type.__new__(cls, name, bases, attrs)


class DSchema(metaclass=DSMeta):
    '''
    Pandas dataframe schema.

    Behavior of schema described by
    class Meta:
        all_required = Bool: All fields changed
                             to requried = True
        exclude_columns = [str]: Columns should not
                                 be present in dataframe
    '''

    def validate(self, df):
        if not isinstance(df, pd.DataFrame):
            raise ValueError('Df must be pandas.dataframe')
        column_dtypes = df.dtypes.to_dict()
        index_dtype = df.index.dtype

        errors = []
        # check exclude
        if self._exclude_columns:
            for column in self._exclude_columns:
                if column in column_dtypes:
                    errors.append('Column %s in exclude list' % column)

        # check index
        if self._df_index and index_dtype != self._df_index.dtype:
            errors.append('Mismatch types for index (%s != %s)' % (self._df_index.dtype, index_dtype))

        # check columns
        for column_name, column in self._df_columns.items():
            required = column.required or self._all_required
            column_dtype = column_dtypes.get(column_name, None)
            if column_dtype is None and required is True:
                errors.append('Required column %s column_name not in df' % column_name)

            if column_dtype is not None and column.dtype != column_dtype:
                errors.append('Wrong dtype %s != %s for column %s' % (column_dtype, column.dtype, column_name))
        return errors

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            raise ValueError()

        if set(self._exclude_columns) != set(other._exclude_columns):
            return False

        if self._all_required != other._all_required:
            return False

        if self._df_index != other._df_index:
            return False

        if set(self._df_columns.keys()) != set(other._df_columns.keys()):
            return False

        for column_name, field in self._df_columns.items():
            if field != other._df_columns[column_name]:
                return False

        return True

    def get_columns(self):
        '''
        :return type: {
                          'required': {'column_name': Field},
                          'not_required': {'column_name': Field},
                      }
        '''
        required_columns = {}
        not_required_columns = {}
        for column_name, field in self._df_columns.items():
            if field.required or self._all_required:
                required_columns[column_name] = field
            else:
                not_required_columns[column_name] = field
        return {
            'required': required_columns,
            'not_required': not_required_columns
        }


    def issuperset(self, other):
        '''
        Возвращает True если описываемый датафрейм можно сузить до
        датафрейма, описываемый схемой other
        '''
        if not set(self._exclude_columns).issubset(set(other._exclude_columns)):
            return False

        if self._df_index.dtype != other._df_index.dtype:
            return False

        self_index_required = self._df_index.required or self._all_required
        other_index_required = other._df_index.required or other._all_required

        if self_index_required is True and other_index_required is False:
            return False

        self_columns = self.get_columns()
        other_columns = self.get_columns()

        self_not_required_columns = self_columns['not_required']
        self_required_columns = self_columns['required']

        other_not_required_columns = other_columns['not_required']
        other_required_columns = other_columns['required']

        # self_requerd_columns > other_required_columns
        if not set(other_required_columns) \
               .issubset(set(self_required_columns)):
            return False

        for column_name in set(other_required_columns) \
                           .issubset(set(self_required_columns)):
            if self_required_columns[column_name] != \
               other_required_columns[column_name]:
                return False

        # not required columns must be equals in intersection
        for column_name in set(self_not_required_columns) \
                           .intersection(set(other_not_required_columns)):
            if self_not_required_columns[column_name] != \
               other_not_required_columns[column_name]:
                return False
        return True

    def squeeze(self, df):
        '''
        Сжимает df до схемы.
        '''
        self.validate(df)
        return df[list(self._df_columns.keys())]



## TESTS:
#
#df = pd.DataFrame([{
#    'float_column': 12.3,
#    'int_column': 12,
#    'str_column': 'str',
#    'datetime_column': datetime(2018, 3, 3),
#}], index=[1])
#
#print(df.dtypes.to_dict())
#
#
## strong df schema
#class MyDF(DSchema):
#
#    class Meta:
#        all_required = False
#        exclude_columns = ['int_column']
#
#    index = DSIndex(data_type='int64')
#    float_column = DSColumn(data_type='float64')
#
#errors = MyDF().validate(df)
#print(errors)

