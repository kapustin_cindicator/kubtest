from abc import ABC, abstractmethod
from ds import DSchema, DSColumn, DSIndex
from datetime import datetime
import attr

class MLSource(ABC):

    @staticmethod
    @abstractmethod
    def params_cls(self):
        pass

    @staticmethod
    @abstractmethod
    def out_schema(self):
        pass

    def __repr__(self):
        return '%s(%s)' % (self.__class__.__name__,
                           self._PARAMS.__repr__())

    def __init__(self, params):
        self._PARAMS = params

    def requirements(self):
        return list()



# SOURCES:

def end_gt_start(instance, attribute, value):
    if value < instance.start:
        raise ValueError('end must be greater then start')

@attr.s(kw_only=True)
class PeriodUserAnswersParams:
    start = attr.ib(validator=attr.validators.instance_of(datetime),
                    default=datetime(2017, 1, 1))
    end = attr.ib(validator=[attr.validators.instance_of(datetime), end_gt_start])


class PeriodUserAnswersOutputSchema(DSchema):

    class Meta:
        all_required = True

    index = DSIndex(data_type='int64')
    event_id = DSColumn(data_type='int64')
    user_id = DSColumn(data_type='int64')
    answer = DSColumn(data_type='float64')
    real = DSColumn(data_type='float64')


class PeriodUserAnswers(MLSource):
    params_cls = PeriodUserAnswersParams
    out_schema = PeriodUserAnswersOutputSchema


@attr.s(kw_only=True)
class UserAnswersParams:
    event_id = attr.ib()

UserAnswersOutputSchema = PeriodUserAnswersOutputSchema


class UserAnswers(MLSource):
    params_cls = UserAnswersParams
    out_schema = UserAnswersOutputSchema

# EXAMPLE:

puas = PeriodUserAnswers(PeriodUserAnswers.params_cls(end=datetime(2019, 3, 3),
                                                      start=datetime(2011, 3, 3)))
print(puas)


uas = UserAnswers(UserAnswers.params_cls(event_id='123124'))
print(uas)
